export class CountriesText {
    text: string;
    secondText: string[];
  
    constructor(text: string, secondText: string[]) {
      this.text = text;
      this.secondText = secondText;
    }
}