import React from 'react';
import logo from './logo.svg';
import './App.css';
import Main from './components/Main';
import img from './img/React.avif'
import TextField from '@mui/material/TextField';

function App() {
  const title = 'Övningsuppgift'
  return (
    <div className='App'>
        <Main title={title}></Main>
    </div>
  );
}

export default App;
