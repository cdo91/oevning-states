import React, { useEffect, useState } from 'react'
import './Main.css'
import Input from './Input'
import CountriesPast from './CountriesPast';
import CountriesFututre from './CountriesFututre';
import { Country } from "./Country";

interface IProps{
    title: string;
}

const Main = (props: IProps) => {
    const titleText = {
        text: 'Vill resa',
        secondText: 'Har rest',
        thirdText: 'Länder jag har rest till',
        fourthText: 'Länder jag vill resa till'
    }

    const countriesPastList = [
        new Country("Kina", true),
        new Country("Usa", true),
        new Country("Spanien", true),
        new Country("Norge", true),
    ];

    const countriesFutureList = [
        new Country("Vietnam", false),
        new Country("Kenya", false),
        new Country("Danmark", false),
        new Country("Kambodya", false),
    ];

    const [updatedPastList, setPast] = useState(countriesPastList);
    const [updatedFutureList, setFuture] = useState(countriesFutureList);
    const [logCountries, setLogCountries] = useState(false);

    const handleAddCountry = (country: Country, addToUpdatedFutureList: boolean) => {
        setLogCountries(true);
        if (addToUpdatedFutureList) {
          if (updatedFutureList.some(x => x.name === country.name)) {
            alert("Landet finns redan i vill resa till listan!");
            return;
          }
          setFuture([...updatedFutureList, country]);
        } else {
          if (updatedPastList.some(x => x.name === country.name)) {
            alert("Landet finns redan i har rest till listan!");
            return;
          }
          setPast([...updatedPastList, country]);
        }
    };
    
    useEffect(() => {
        if (logCountries) {
            console.log(updatedPastList)
            console.log(updatedFutureList)
        }
    });

    return <div>
     
      <h1>{props.title}</h1>

      <div className="container">
          <Input onAddCountry={handleAddCountry} text={titleText.text} secondText={titleText.secondText}/>
          <CountriesPast pastText={titleText.thirdText} countriesPastList={updatedPastList}/>
          <CountriesFututre futureText={titleText.fourthText} countriesFutureList={updatedFutureList}/>
      </div>

    </div>
}

export default Main;