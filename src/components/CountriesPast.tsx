import React, { useState } from 'react'
import './CountriesPast.css'
import { Country } from './Country';

interface ICountry{
  pastText: string;
  countriesPastList: Country[]
}

const CountriesPast = ({pastText, countriesPastList} : ICountry) => {

  const [showLists, setShowLists] = useState(false);

  const toggleShowLists = () => {
    setShowLists(!showLists);
  }
  
  return (
    <div>
      <ul><p className='p1' onClick={toggleShowLists}>{pastText}</p>
        {showLists && countriesPastList.map(country => (
          <li>- {country.name}</li>
        ))}
      </ul>
    </div>
  )
}

export default CountriesPast