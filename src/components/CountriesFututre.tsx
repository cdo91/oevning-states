import React, { useState } from 'react'
import './CountriesFuture.css'
import { Country } from "./Country"

interface ICountry{
  futureText: string;
  countriesFutureList: Country[]
}

const CountriesFututre = ({futureText, countriesFutureList} : ICountry) => {

  const [showLists, setShowLists] = useState(false);

  const toggleShowLists = () => {
    setShowLists(!showLists);
  }

  return (
    <div>
      <ul><p onClick={toggleShowLists}>{futureText}</p>
        {showLists && countriesFutureList.map(country => (
          <li>- {country.name}</li>
        ))}
      </ul>
    </div>
  )
}

export default CountriesFututre