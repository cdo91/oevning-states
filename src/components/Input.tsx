import React, { useState } from 'react'
import './Input.css'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import { Country } from "./Country"

interface IProps{
  text: string;
  secondText: string;
  onAddCountry: (country: Country, addToUpdatedFutureList: boolean) => void;
}

const Input = ({text, secondText, onAddCountry}: IProps) => {

  const [newCountry, setNewCountry] = useState('');
  
  const [futureChecked, setIsFutureChecked] = useState(false);
  const [pastChecked, setIsPastChecked] = useState(false);

  const textFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewCountry(event.target.value.charAt(0).toUpperCase() + event.target.value.slice(1));
  };
  

  const futureCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIsFutureChecked(event.target.checked);
  };
  
  const pastCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIsPastChecked(event.target.checked);
  };

  const buttonClick = () => {
    if (!newCountry.length) {
      alert('Fältet för länder måste vara ifyllt');
      return;
    }
    if (futureChecked === true && pastChecked === true || futureChecked === false && pastChecked === false) {
      alert('Välj antingen "Vill resa" eller "Har rest"');
      return;
    }
    if (futureChecked) {
      onAddCountry({
        name: newCountry,
        visited: false
      }, true);
    } else {
      onAddCountry({
        name: newCountry,
        visited: true
      }, false);
    }
  };

  return (
    <div>
      <div className="search-container">
        <TextField onChange={textFieldChange} value={newCountry} sx={{mr: 0.5}} size="small" className='input' id="outlined-basic" label="Lägg till.." variant="outlined" />
        <Button onClick={(buttonClick)} sx={{fontWeight: 'bold'}} color="secondary" variant="contained">Ok</Button>
      </div>

      <div className="checkbox">
        <Checkbox onChange={futureCheckboxChange} color="secondary"/>
        {text}
      </div>

      <div className='checkbox'>
        <Checkbox onChange={pastCheckboxChange} color="secondary"/>
        {secondText}
      </div>
    </div>
  )
}

export default Input